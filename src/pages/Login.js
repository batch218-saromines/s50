import {useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Login() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);



    function authenticateUser(e) {
        e.preventDefault();
        setEmail("");
        setPassword("");

        alert("You are now logged in.");
        console.log("Logged in successful");


    }

    useEffect(() => {

        if (password === "" || email.length === "") {
            setIsActive(false);
            return;
        }

        setIsActive(true);



    },[email, password])


    return (
        <>
        <h1>Login</h1>
        <Form onSubmit={(e) => authenticateUser(e)}>
            <Form.Group  controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                required
                    value={email}
                    onChange={e => {
                        setEmail(e.target.value);
                    }}
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                required
                    value={password}
                    onChange={e => {
                        setPassword(e.target.value);
                    }}
                />
            </Form.Group>

            <Button className="mt-3" variant="success" type="submit" id="submitBtn" 
            disabled={!isActive}
            >
            	Submit
            </Button>
        </Form>

        </>
    )

}
